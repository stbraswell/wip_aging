import csv
import requests
import json
import os
import datetime
import time
import sys
import glob
import thread
import threading
from threading import Thread
import os.path
import traceback
import re
import collections
from shutil import copy
from lxml import html
import math
import openpyxl
import smtplib
import itertools
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email.MIMEImage import MIMEImage
from email import encoders
from openpyxl import Workbook
from selenium import webdriver
from selenium.webdriver import ChromeOptions 
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

###################################################################
# Downloads manufacturing WIP Detail file, puts units into buckets based on unit type,location and age.
# Generates HTML for email as well as for the supporting interactive graphs taht are served up by apache.
# The Main email contains images of the graphs that contain a link to the interactive graphs.
# The inteactive graphs show the number of units in each location, with each column "stacked" to show the number of
# units within that location at various ages.
# The User can click on an age within a location to see detailed info for each unit within that age group and location. 


############################## VARS ##############################
## Get arg, if provided
try:
	searchTerm = str(sys.argv[1])
except:
	searchTerm = 'FULL'

today_str = datetime.datetime.today().strftime('%m-%d-%Y')

# Set base path to script
base = os.path.dirname(__file__)	# This sets "base" when script is started via a scheduler
if not base:						# This sets "base" when script is started manually
	base=os.getcwd()

resultFolder= base +'/Results'
todaysFolder = resultFolder + '/%s' %today_str
todaysOutput= todaysFolder +'/_output'
dlFolder= todaysFolder + '/_input'
savefile = dlFolder + '/Wip_Debug_Report_B3.xlsx'
wipDetail = dlFolder + '/1_WIP_Detail.xlsx'
bucketFile_asy = base + '/_input/buckets_asy_update2.csv'	# Defines production location : category relationship for assemblies
bucketFile_pca = base + '/_input/buckets_pca.csv'			# Defines production location : category relationship for pca's

################NEW VARS###################
firstAsyScanList = []
firstPcaScanList = []
dictAsyByAging={}
dictPcaByAging={}
fileList=[]
bucketList_asy = collections.defaultdict(list)
bucketList_pca = collections.defaultdict(list)
dictAsyByLoc = collections.defaultdict(list)
dictPcaByLoc = collections.defaultdict(list)
detail_html_file = todaysOutput + '/%s_%s.html'
main_html_file = todaysOutput + '/Main_%s_%s.html'
detail_html_file_loc = todaysOutput + '/%s_%s_%s.html'
main_html_file_loc = todaysOutput + '/Main_Loc_%s_%s.html'
gapindays=45
timeBuckets =['0-89','90-139','140-179','180-269','270+']
b3GoodWip = ['FVT,ESS,BURN,ORT1,MEA2, Hipot, FST ','Heat, Mechanical, IQC1 ','Arnet Van','FQC, PACK ','SHIP, ARGF ',' B3 Good WIP','Debug, Rework']
b3GoodWip2 = ['ABRT Burn Retest',
			'AERT ESS Retest',
			'AESS','AFRT FVT Retest',
			'AFSR FST Retest',
			'AFST',
			'AFVT Functional Test',
			'BURN TEST BURN IN',
			'HIPO TEST HI POT',
			'MEA2 Mechanical 2'
			,'ORT1 TEST ORTT 1'
			,'HEAT SINK'
			,'IQC1 In Process QC 1'
			,'MEAS Mechanical'
			,'MST1 Mech Station 1'
			,'MST2 Mech Station 2',
			'MST3 Mech Station 3']
b3GoodWipAging=['180-269','270+']
b2GoodWipAging_file = todaysOutput + '/B3_Good-WIP_aging.csv'
wipDetailURL='https://internalsite.com/to/wip_detail'
username = "username"
password = "password"
# Define XPaths
xpaths = { 'usernameTxtBox' : '//*[@id="username"]',
			'passwordTxtBox' : '//*[@id="password"]',
			'loginBtn' : '//*[@id="Submit"]',
			'gobtn' : "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div[1]//div[2]/button[2]/span",
			'xlsxbtn' : "/html/body/div[3]/div/ul/li[2]/a",
			'dropdownbtn' : "//*[@id='container']/div[1]/div/div/div[2]/div[2]/div/div/div[1]/span[1]/button[2]/span[1]"}

############################## FUNCS ##############################			
# Download WIP Detail CSV
def getWIPDetail(url): #,report):
	try:
		# Setup ChromeDriver
		chromedriver_path = "C:/Python27/chromedriver.exe"
		chrome_options = Options() 
		chrome_options.add_argument("--log-level=3")	# Reduces amount of output to console
		# Set path for downloaded files
		prefs = {"download.default_directory" : dlFolder}
		chrome_options.add_experimental_option("prefs",prefs)
		chrome_options.binary_location = "C:/path/to/chrome.exe"
		mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
		# Open the URL
		mydriver.get(url)
		# Tells the program to wait for 10 seconds before throwing an exception
		mydriver.implicitly_wait(10)
		# Used for explicit waits, program will wait for 900 seconds before throwing exception, while continuously looking for an element
		wait = WebDriverWait(mydriver, 900)
		# Clear Username TextBox if already allowed "Remember Me" 
		mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).clear()
		# Write Username in Username TextBox
		mydriver.find_element_by_xpath(xpaths['usernameTxtBox']).send_keys(username)
		# Clear Password TextBox if already allowed "Remember Me" 
		mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).clear()
		# Write Password in password TextBox
		mydriver.find_element_by_xpath(xpaths['passwordTxtBox']).send_keys(password)
		# Click Login button
		mydriver.find_element_by_xpath(xpaths['loginBtn']).click()
		# Wait until the dropdown menu is clickable or for 30 seconds	
		dropdownelement = wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='container']/div[1]/div/div/div[2]/div[3]/div/div[1]/div[1]/div[1]/div/div/select/option[2]")))
		# Click the dropdown menu
		dropdownelement.click()
		time.sleep(1)
		mydriver.find_element_by_xpath(xpaths['gobtn']).click()
		dropdownelement = wait.until(EC.element_to_be_clickable((By.XPATH, "//*[@id='container']/div[1]/div/div/div[2]/div[2]/div/div/div[1]/span[1]/button[2]/span[1]")))
		# Click the dropdown menu
		dropdownelement.click()
		# Click on "CSV" within the dropdown menu
		mydriver.find_element_by_xpath(xpaths['xlsxbtn']).click()
		# wait for 5 seconds. This may not be needed, but gives time for the file to download
		time.sleep(5)
		# Wait for file to completely download
		while not glob.glob(dlFolder + '/Wip_Detail*.xlsx'):
			print 'Still Downloading'
			time.sleep(5)
		# Close the window
		mydriver.quit()
		# Move file and rename
		# Check if previous wipDetail file exists, if so delete it
		if os.path.exists(wipDetail):
			os.remove(wipDetail)
		# Get the name of the latest "Wip_Detail-XXXXX.xlsx" file
		file = max(glob.iglob( dlFolder + '/Wip_Detail*.xlsx'), key=os.path.getctime)
		# Rename the just downloaded file 
		os.rename(file, wipDetail )
	except:
		# Get the traceback object
 		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		print pymsg
 		# Close the Browser Window
 		mydriver.quit()
	
def emailit(body):
	fromaddr = "troy.braswell@example.com"
	toaddr = ["troy.braswell@example.com"]

	pfile= 'C:/path/to/pfile.txt'
	msg = MIMEMultipart('alternative')
	subject= 'WIP Aging %s' % today_str
	msg['From'] = fromaddr
	msg['To'] = ", ".join(toaddr) #toaddr
	msg['Subject'] = subject
	file = open(pfile, 'rb') 
	pword= file.read() 
	text = "You found Waldo!  Great Job! -Troy"
	part1 = MIMEText(text, 'plain')
	part2 = MIMEText(body, 'html')
	msg.attach(part1)
	msg.attach(part2)
	
	# Create image 1 object and attach to message
	fp = open(todaysOutput + '/WIP_asy.jpg', 'rb')
	msgImage = MIMEImage(fp.read())
	fp.close()
	# Define the image's ID as referenced above
	msgImage.add_header('Content-ID', '<asyimg>')
	msg.attach(msgImage)
	
	# Create image 2 object and attach to message
	fp = open(todaysOutput + '/WIP_pca.jpg', 'rb')
	msgImage = MIMEImage(fp.read())
	fp.close()
	# Define the image's ID as referenced above
	msgImage.add_header('Content-ID', '<pcaimg>')
	msg.attach(msgImage)
	
	# Create image 3 object and attach to message
	fp = open(todaysOutput + '/WIP_Loc_asy.jpg', 'rb')
	msgImage = MIMEImage(fp.read())
	fp.close()
	# Define the image's ID as referenced above
	msgImage.add_header('Content-ID', '<asylocimg>')
	msg.attach(msgImage)
	
	# Create image 4 object and attach to message
	fp = open(todaysOutput + '/WIP_Loc_pca.jpg', 'rb')
	msgImage = MIMEImage(fp.read())
	fp.close()
	# Define the image's ID as referenced above
	msgImage.add_header('Content-ID', '<pcalocimg>')
	msg.attach(msgImage)
	
	# Provide all info and send the email 
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, pword)
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()

def emailB3WipAging():
	fromaddr = "troy.braswell@example.com"
	toaddr = ["troy.braswell@example.com"]
	pfile= 'C:/path/to/pfile.txt'
	msg = MIMEMultipart()

	date=time.strftime("%d-%m-%Y %I:%M")
	subject= 'B3 Good WIP Aging %s' % date
	msg['From'] = fromaddr
	msg['To'] = ", ".join(toaddr) 
	msg['Subject'] = subject

	file = open(pfile, 'rb') 
	pword= file.read() 

	body = "Hi All, \nI have attached the latest list of aging units in B3 good WIP. \n\nThanks,\nTroy" 
	 
	msg.attach(MIMEText(body, 'plain'))
	 
	filename = 'B3_Good-WIP_aging.csv'
	attachment = open(b2GoodWipAging_file, "rb")
	 
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
	 
	msg.attach(part)
	 
	server = smtplib.SMTP('smtp.gmail.com', 587)
	server.starttls()
	server.login(fromaddr, pword)
	text = msg.as_string()
	server.sendmail(fromaddr, toaddr, text)
	server.quit()
	
# Generates html for interactive graph based on AGE
def generateHTML(dict2parse,parttype): #(asy or pca dictionary, "asy" or "pca")
	html="<!DOCTYPE html>\n\
	<html lang='en-US'>\n\
	<head>\n\
	<title>WIP AGING - %s</title>\n\
	<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>\n\
	<script type='text/javascript'>\n\
		google.charts.load('current', {packages: ['corechart']});\n\
	</script>\n\
	</head>\n\
	<body>\n\
	<div id='container' style='width: 1900px; height: 800px; margin: 0 auto'></div>\n\
	<script language='JavaScript'>\n\
		function drawChart() {\n\
			var data = google.visualization.arrayToDataTable([\n\
				['Days','Qty']" % parttype.upper()
	i=0
	
	for item in timeBuckets:
		quantity=len(dict2parse[item])
		html = html + ",\n				['%s',%d]" % (item,quantity)
		
	html = html + "\n			]);\n\
			var options = {\n\
				title: '%s WIP Aging',\n\
				hAxis: {\n\
					title: 'Days',\n\
					showTextEvery:1,\n\
					slantedText:true,\n\
					slantedTextAngle:45\n\
				},\n\
				vAxis: {\n\
					title: 'Number of Units',\n\
					gridlines: { count: 10 }\n\
				}\n\
			};\n\
			var chart = new google.visualization.ColumnChart(document.getElementById('container'));\n\
			function selectHandler() {\n\
				var selectedItem = chart.getSelection()[0];\n\
				if (selectedItem) {\n\
					var topping = data.getValue(selectedItem.row, 0);\n\
					window.location.href = 'http://sjc1amwk499303:8887/%s/_output/%s_'+topping + '.html';\n\
				}\n\
			}\n\
			google.visualization.events.addListener(chart, 'select', selectHandler);\n\
			chart.draw(data, options);\n\
		}\n\
		google.charts.setOnLoadCallback(drawChart);\n\
	</script>\n\
	</body>\n\
	</html>\n" % (parttype.upper(),today_str,parttype)

	i=0
	for idx,ele in enumerate(dict2parse):	#{DayGroup:[[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),DayGroup]],...}
		days='%s-%s' % (i,(i+14))
		temp_html = '<html>\
		<head>\
		<title>Serial Number Info</title></head>\
				<br><b><p style="font-size:15px">Units Aging between %s Days:</b></p>\
							<table style="border: 1px solid black ;border-collapse:collapse;">\
								<tr style="background-color: #CFCFA3;"> \
									<th style="border: 1px solid black ;">Shop Order</th>\
									<th style="border: 1px solid black ;">Part Number</th>\
									<th style="border: 1px solid black ;">Description</th>\
									<th style="border: 1px solid black ;">SN</th>\
									<th style="border: 1px solid black ;">Status</th>\
									<th style="border: 1px solid black ;">WIP Location</th>\
									<th style="border: 1px solid black ;">First Scan</th>\
									<th style="border: 1px solid black ;">Last Scan</th>\
									<th style="border: 1px solid black ;">Days Since Last Scan</th>\
									<th style="border: 1px solid black ;">Days Since First Scan</th>\
								</tr>' %ele
		color_switch=0
		for item in dict2parse[ele]:	
			for indx,thing in enumerate(item):
				if indx in [1,6,12]:
					continue
				if color_switch == 0:
					if indx == 0:
						temp_html = temp_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black">%s</td>' % (thing)
					elif indx < (len(item)-1):
						temp_html = temp_html + '<td style="border: 1px solid black">%s</td>' % (thing) #.encode('utf-8'))
					else:
						temp_html = temp_html + '<td style="border: 1px solid black">%s</td></tr>' % (thing)#.encode('utf-8'))
				else:
					if indx == 0:
						temp_html = temp_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black">%s</td>' % (thing)
					elif indx < (len(item)-1):
						temp_html = temp_html + '<td style="border: 1px solid black">%s</td>' % (thing)#.encode('utf-8'))
					else:
						temp_html = temp_html + '<td style="border: 1px solid black">%s</td></tr>' % (thing)#.encode('utf-8'))
			if color_switch == 0:
				color_switch += 1
			else:
				color_switch -= 1
		temp_html = temp_html + "</table></html>"
		f = open(detail_html_file % (parttype,ele), 'w' )
		f.write(temp_html)
		f.close()
		i+=15
		
	f = open(main_html_file % (parttype,today_str), 'w' )
	f.write(html)
	f.close()
	fileList.append('file:///C:/path/to/scripts/Wip_aging/Results/%s/_output/Main_%s_%s.html' %(today_str,parttype,today_str))

# Generates html for interactive graph based on LOCATION
def generateLocHTML(dict2parse,parttype): #{Loc1:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]},Loc2:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]}}
	html="<!DOCTYPE html>\n\
	<html lang='en-US'>\n\
	<head>\n\
	<title>WIP AGING by Location - %s</title>\n\
	<script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>\n\
	<script type='text/javascript'>\n\
		google.charts.load('current', {packages: ['corechart']});\n\
	</script>\n\
	</head>\n\
	<body>\n\
	<div id='container' style='width: 2300px; height: 900px; margin: 0 auto'></div>\n\
	<script language='JavaScript'>\n\
		function drawChart() {\n\
			var data = google.visualization.arrayToDataTable([\n\
				['Location'" % parttype.upper()
	for item in timeBuckets:
		html = html + ",'%s'" %item
		
	html = html + ']'
	for location in dict2parse:	#{Loc1:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]},Loc2:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]}}
		tmprow=[location]
		for bkt in timeBuckets:
			if bkt in dict2parse[location]:
				tmprow.append(len(dict2parse[location][bkt]))
			else:
				tmprow.append(0)
		html = html + ',\n				%s' % tmprow
		
	html = html + "\n			]);\n\
			var options = {\n\
				title: '%s WIP Aging by Location',\n\
				hAxis: {\n\
					title: 'Location',\n\
					showTextEvery:1,\n\
					slantedText:true, \n\
					slantedTextAngle:45\n\
					},\n\
				vAxis: {\n\
					title: 'Number of Units',\n\
					gridlines: { count: -1 }\n\
				},\n\
				explorer: {\n\
					actions: ['dragToZoom', 'rightClickToReset'],\n\
					axis: 'vertical',\n\
					keepInBounds: true,\n\
					maxZoomIn: .01\n\
					},\n\
				isStacked: true,\n\
				colors:['green','blue','yellow','orange','red']\n\
			};\n\
			var chart = new google.visualization.ColumnChart(document.getElementById('container'));\n\
			function selectHandler() {\n\
				var selectedItem = chart.getSelection()[0];\n\
				if (selectedItem) {\n\
					var topping = data.getValue(selectedItem.row, 0);\n\
					var test = data.getColumnLabel(selectedItem.column,0);\n\
					window.location.href = 'http://sjc1amwk499303:8887/%s/_output/%s_'+ topping + '_' + test + '.html';\n\
				}\n\
			}\n\
			google.visualization.events.addListener(chart, 'select', selectHandler);\n\
			chart.draw(data, options);\n\
		}\n\
		google.charts.setOnLoadCallback(drawChart);\n\
	</script>\n\
	</body>\n\
	</html>" % (parttype.upper(),today_str,parttype)

	for idx,location in enumerate(dict2parse):	#Location #{Loc1:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]},Loc2:{AgeGroup1:[[row],[row],[row]],AgeGroup2:[[row],[row]]}}
		for numb,agegroup in enumerate(dict2parse[location]):	# Agegroup
			temp_html = '<html>\
			<head>\
			<title>SN Info</title></head>\
					<br><b><p style="font-size:15px">%s Aging between %s Days:</b></p>\
								<table style="border: 1px solid black ;border-collapse:collapse;">\
									<tr style="background-color: #CFCFA3;"> \
										<th style="border: 1px solid black ;">Shop Order</th>\
										<th style="border: 1px solid black ;">Part Number</th>\
										<th style="border: 1px solid black ;">Description</th>\
										<th style="border: 1px solid black ;">SN</th>\
										<th style="border: 1px solid black ;">Status</th>\
										<th style="border: 1px solid black ;">WIP Location</th>\
										<th style="border: 1px solid black ;">First Scan</th>\
										<th style="border: 1px solid black ;">Last Scan</th>\
										<th style="border: 1px solid black ;">Days Since Last Scan</th>\
										<th style="border: 1px solid black ;">Days Since First Scan</th>\
									</tr>' %(location,agegroup)
			color_switch=0
			for item in dict2parse[location][agegroup]:	# item within agegroup
				for indx,thing in enumerate(item):
					if indx in [1,6,12]:
						continue
					if color_switch == 0:
						if indx == 0:
							temp_html = temp_html + '<tr style="background-color: #FFFFDB;"><td style="border: 1px solid black">%s</td>' % (thing)
						elif indx < (len(item)-1):
							temp_html = temp_html + '<td style="border: 1px solid black">%s</td>' % (thing) #.encode('utf-8'))
						else:
							temp_html = temp_html + '<td style="border: 1px solid black">%s</td></tr>' % (thing)#.encode('utf-8'))
					else:
						if indx == 0:
							temp_html = temp_html + '<tr style="background-color: #CFCFA3;"><td style="border: 1px solid black">%s</td>' % (thing)
						elif indx < (len(item)-1):
							temp_html = temp_html + '<td style="border: 1px solid black">%s</td>' % (thing)#.encode('utf-8'))
						else:
							temp_html = temp_html + '<td style="border: 1px solid black">%s</td></tr>' % (thing)#.encode('utf-8'))
				if color_switch == 0:
					color_switch += 1
				else:
					color_switch -= 1
			temp_html = temp_html + "</table></html>"
			
			# Create detail html file
			f = open(detail_html_file_loc % (parttype,location,agegroup), 'w' )
			f.write(temp_html)
			f.close()		
	# Create main html file	
	f = open(main_html_file_loc % (parttype,today_str), 'w' )
	f.write(html)
	f.close()
	fileList.append('file:///C:/path/to/scripts/Wip_aging/Results/%s/_output/Main_Loc_%s_%s.html' %(today_str,parttype,today_str))
	
# Generate HTML for Email
def getEmailHtml():
	html4email='<html>\
			Hi All,<br>\
			<br>'
	html4email = html4email+ '<p><b>5/15/2018 - UPDATE:</b> Age is now calculated from the Monday of the week each unit was built, instead of from the first scan.<br><br>\
	Please see links below for details (A Wired connection is required to view links):<br></p>\
	<div style="display:inline-block;">\
	<div style="float: left;margin-right: 20px;">\
	<p><b>Asy WIP Aging</b></p>\
	<a href="http://sjc1amwk499303:8887/%s/_output/Main_asy_%s.html">\
	<img src="cid:asyimg" alt="HTML5 Icon" style="width:256px;height:128px;">\
	</a></div>' % (today_str,today_str)
	
	html4email = html4email +'\
	<div style="float: left;">\
	<p><b>PCA WIP Aging</b></p>\
	<a href="http://sjc1amwk499303:8887/%s/_output/Main_pca_%s.html">\
	<img src="cid:pcaimg" alt="HTML5 Icon" style="width:256px;height:128px;">\
	</a></div></div>' % (today_str,today_str)
	
	html4email = html4email+ '\
	<div style="display:inline-block;">\
	<div style="float: left;margin-right: 20px;">\
	<p><b>Asy WIP Aging by Location</b></p>\
	<a href="http://sjc1amwk499303:8887/%s/_output/Main_Loc_asy_%s.html">\
	<img src="cid:asylocimg" alt="HTML5 Icon" style="width:256px;height:128px;">\
	</a></div>' % (today_str,today_str)
	
	html4email = html4email +'\
	<div style="float: left;">\
	<p><b>PCA WIP Aging by Location</b></p>\
	<a href="http://sjc1amwk499303:8887/%s/_output/Main_Loc_pca_%s.html">\
	<img src="cid:pcalocimg" alt="HTML5 Icon" style="width:256px;height:128px;">\
	</a></div></div>' % (today_str,today_str)
		
	html4email = html4email+ '<br><br><div style="display:inline-block;">\
		Thanks,<br>\
		Troy<br></div> \
		<p style=" position: absolute; bottom: 0; left: 0; width: 100%%; \
		text-align: center; font-size: 0.3em;">#AutomatedEmail</p></html>'
		
	return html4email
	
def getScreenshots(listofiles):
	try:
		# Setup ChromeDriver
		chromedriver_path = "C:/Python27/chromedriver.exe"
		chrome_options = Options() 
		chrome_options.add_argument("--log-level=3")
		# Set path for downloaded files
		prefs = {"download.default_directory" : dlFolder}
		chrome_options.add_experimental_option("prefs",prefs)
		chrome_options.binary_location = "C:/path/to/chrome.exe"
		mydriver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)
		# Tells the program to wait for 10 seconds before throwing an exception
		mydriver.implicitly_wait(10)
		# Used for explicit waits, program will wait for 20 seconds before throwing exception, while continuously looking for an element
		wait = WebDriverWait(mydriver, 20)
		for item in listofiles:
			myid = item.split('/')[-1].split('_')[1]
			if myid == 'Loc':
				myid = myid + '_' + item.split('/')[-1].split('_')[2]
			mydriver.get(item)
			# locate element
			element = mydriver.find_element_by_xpath('/html/body/div[1]')
			# get element size
			size = element.size
			# Get element Location
			location = element.location
			# Set window size to element size
			mydriver.set_window_size(size['width'], (size['height'] + 150))
			# Save screenshot
			mydriver.get_screenshot_as_file(todaysOutput + '/WIP_%s.jpg' % myid) 
		# Close the window
		mydriver.quit()

	except:
		# Get the traceback object
 		tb = sys.exc_info()[2]
 		tbinfo = traceback.format_tb(tb)[0]
 		# Concatenate information together concerning the error into a message string
		pymsg = "PYTHON ERRORS:\nTraceback info:\n" + tbinfo + "\nError Info:\n" + str(sys.exc_info()[1])
		print pymsg
 		# Close the Browser Window
 		mydriver.quit()
		
############################## MAIN ##############################

# Create today's folder and subfolders if it doesn't already exist
if not os.path.exists(todaysFolder):
	os.makedirs(todaysFolder)
	os.makedirs(dlFolder)
	os.makedirs(todaysOutput)

# DL WIP Report
if searchTerm in ['FULL']:
	print 'Downloading WIP Detail'
	getWIPDetail(wipDetailURL)
	print 'Done!'

# Load Buckets into dictionaries:
with open(bucketFile_asy, 'rb') as f:	# ASSEMBLIES
	mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
	mycsv = list(mycsv)
for row in mycsv:		
	bucketList_asy[row[0]].append(row[1]) #{loc1:[Group1],loc2:[Group2]...}	

with open(bucketFile_pca, 'rb') as f:	# PCAs
	mycsv = csv.reader(x.replace('\0', '') for x in f) # This fixes the 'Null byte' issue
	mycsv = list(mycsv)
for row in mycsv:		
	bucketList_pca[row[0]].append(row[1]) #{loc1:[Group1],loc2:[Group2]...}
	
# Load WIP_Detail.xlsx
wb = openpyxl.load_workbook(wipDetail)
ws=wb['wip_detail']

print 'Categorizing Units'

# loop over rows in WIP_Detail, add to array corresponding to the process that the unit is currently in
for row in ws.iter_rows(min_row=2,min_col=1,max_col=12): 
	if ('LFARISASY' in row[2].value) and (row[7].value not in ['NOT-IN-PROCESS']):
		firstAsyScanList.append([cell.value.encode('UTF-8') for cell in row]) #[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days)]
	if ('LFARISPCA' in row[2].value) and (row[7].value not in ['NOT-IN-PROCESS']):
		firstPcaScanList.append([cell.value.encode('UTF-8') for cell in row]) #[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days)]

# Convert age of ASSY to be today - the monday of the week it was built (per Production Manager request)
for item in firstAsyScanList:
	unitYear = re.search("(?<=[A-Z]{3})([0-9]{2})",item[4]).group()
	unitWeek = re.search("(?<=.....)([0-9]{2})",item[4]).group()
	birthday = datetime.datetime.strptime('20' + unitYear + '-W' + unitWeek + '-0', "%Y-W%W-%w")
	rightNow = datetime.datetime.today()
	unitAge = (rightNow - birthday).days
	item[-1] = unitAge

# Sort ASSY list of list by age, from oldest to youngest
firstAsyScanList = sorted(firstAsyScanList,key = lambda x:x[-1],reverse=True)

# Convert age of PCA to be today - the monday of the week it was built
for item in firstPcaScanList:
	unitYear = re.search("(?<=[A-Z]{3})([0-9]{2})",item[4]).group()
	unitWeek = re.search("(?<=.....)([0-9]{2})",item[4]).group()
	birthday = datetime.datetime.strptime('20' + unitYear + '-W' + unitWeek + '-0', "%Y-W%W-%w")
	rightNow = datetime.datetime.today()
	unitAge = (rightNow - birthday).days
	item[-1] = unitAge
	
# Sort PCA list of list by age, from oldest to youngest
firstPcaScanList = sorted(firstPcaScanList,key = lambda x:x[-1],reverse=True)

# Sort Assembly units into their corresponding age category (aka 'timeBuckets')
for item in firstAsyScanList:
	for idx,ele in enumerate(timeBuckets[:-1]):
		low = int(ele.split('-')[0])
		high = int(ele.split('-')[1])
		if (int(item[-1]) >= low) and (int(item[-1]) < high):			# check if age (item[-1]) is within range of timeBucket
			item.append(ele)											# Add timeBucket name to list
			if item[7] in bucketList_asy:								# Check if the unit location (item[7]) is a location we want to know about
				dictAsyByLoc[bucketList_asy[item[7]][0]].append(item)	# If so, add to Assembly by location dictionary (dictAsyByLoc[location bucket]=[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days)]
			else:
				dictAsyByLoc['UNKNOWN'].append(item)					# If it is an unknown location then add to "UNKNOWN" category
			break
		elif int(item[-1]) >= 270:										# If the unit didnt fit into the ages above, check if it is over 270 days old
			item.append('270+')
			if item[7] in bucketList_asy:
				dictAsyByLoc[bucketList_asy[item[7]][0]].append(item)
			else:
				dictAsyByLoc['UNKNOWN'].append(item)
			break


# Create a dictionary whose keys are the age groups.  The values for each key is a list of lists.  This makes it easier to order the units when looping over the keys and values to create the html.
f = lambda x: x[-1]
for key, group in itertools.groupby(sorted(firstAsyScanList, key=f), f):	#[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),firstscanGroup]
    dictAsyByAging[key] = list(group)	#{DayGroup:[[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),DayGroup]],...}

# Sort list of lists for each location by the number of units in each Work order
dictAsyByLoc=collections.OrderedDict(sorted(dictAsyByLoc.items(), key=lambda x: len(x[1]), reverse=True))

# Assy Location by Aging
f = lambda x: x[-1]
for item in dictAsyByLoc:
	dictTemp={}
	for key, group in itertools.groupby(sorted(dictAsyByLoc[item], key=f), f):	#[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),firstscanGroup]
		dictTemp[key] = list(group)	#{DayGroup:[[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),DayGroup]],...}	
	dictAsyByLoc[item]=dictTemp

with open(b2GoodWipAging_file, 'wb') as f:
	clusterwriter = csv.writer(f)
	clusterwriter.writerow(['WO Number','WIP QTY','PartNumber','PartDescription','SerialNumber','Status','WIP Group','WIP Location','First Scan','Most Recent Scan','Age Since Last Transaction (days)','Age From First Scan (days)','Aging Bucket'])
	for key in dictAsyByLoc:
		if key in b3GoodWip:
			for key2 in dictAsyByLoc[key]:
				if key2 in b3GoodWipAging:
					for item in dictAsyByLoc[key][key2]:
						clusterwriter.writerow(item)

# Sort PCA units into their corresponding age category (aka 'timeBuckets')
for item in firstPcaScanList:
	for idx,ele in enumerate(timeBuckets):
		if (int(item[-1]) >= (gapindays * idx)) and (int(item[-1]) < (gapindays * (idx + 1))):
			item.append(ele)
			if item[7] in bucketList_pca:
				dictPcaByLoc[bucketList_pca[item[7]][0]].append(item)
			else:
				dictPcaByLoc['UNKNOWN'].append(item)
			break
		elif int(item[-1]) >= 270:
			item.append('270+')
			if item[7] in bucketList_pca:
				dictPcaByLoc[bucketList_pca[item[7]][0]].append(item)
			else:
				dictPcaByLoc['UNKNOWN'].append(item)
			break	#{Loc1:[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days)],Loc2:[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days)]}

f = lambda x: x[-1]
for key, group in itertools.groupby(sorted(firstPcaScanList, key=f), f):
    dictPcaByAging[key] = list(group)	#{DayGroup:[[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),DayGroup]],...}

dictPcaByLoc=collections.OrderedDict(sorted(dictPcaByLoc.items(), key=lambda x: len(x[1]), reverse=True))
	
# Sort PCA Location Aging
f = lambda x: x[-1]
for item in dictPcaByLoc:
	dictTemp={}
	for key, group in itertools.groupby(sorted(dictPcaByLoc[item], key=f), f):	#[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),firstscanGroup]
		dictTemp[key] = list(group)	#{DayGroup:[[WO Number, WIP QTY, PartNumber, PartDescription, SerialNumber, Status, WIP Group, WIP Location, First Scan, Most Recent Scan, Age Since Last Transaction (days), Age From First Scan (days),DayGroup]],...}	
	dictPcaByLoc[item]=dictTemp


generateHTML(dictAsyByAging,'asy')		# Create HTML for Assembly units by age
generateHTML(dictPcaByAging,'pca')		# Create HTML for PCA units by age
generateLocHTML(dictAsyByLoc,'asy')		# Create HTML for Assembly units by location
generateLocHTML(dictPcaByLoc,'pca')		# Create HTML for PCA units by location

getScreenshots(fileList)				# Generate jpg images for email

emailHTML = getEmailHtml()				# Create Full Email HTML

emailit(emailHTML)	# Email Results
emailB3WipAging()	# Email BLDG 3 numbers to BLDG 3 Managers